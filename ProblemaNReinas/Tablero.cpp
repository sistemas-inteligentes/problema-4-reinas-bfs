#include "Tablero.h"
#include <time.h>

Tablero::Tablero()
{
	Explorado = false;
	CasillaAuxiliar.first = 0;
	CasillaAuxiliar.second = 0;

	for (int i = 1; i < TAM; i++)
	{
		for (int j = 1; j < TAM; j++)
		{
			CasillaAuxiliar.first = i;
			CasillaAuxiliar.second = j;
			Validos.push_back(CasillaAuxiliar);
		}
	}
}

Tablero::~Tablero()
{
}


void Tablero::mandarReinas(int InicioVector[TAM])
{

	for (int i = 1; i < TAM; i++)
	{
		vectorR[i] = InicioVector[i];
	}

}

int Tablero::getTamValidos()
{
	return Validos.size();
}

int Tablero::getReina(int columna)
{
	return vectorR[columna];
}

bool Tablero::EsSolucion()
{
	bool resp = true;
	for (int i = 1; i < TAM; i++)
	{
		if (vectorR[i] == 0)
		{
			return false;
		}
	}
	return resp;
}

bool Tablero::NoRepetido(int VectorParaVerificar[TAM])
{
	bool resp = true;
	int i = 1;
	while (i < TAM)
	{
		if (vectorR[i] != VectorParaVerificar[i])
		{
			return false;
		}
		i++;
	}
	return resp;
}

bool Tablero::ValidosMayoresAReinas()
{
	int NroReinas = 0;
	for (int i = 1; i < TAM; i++)
	{
		if (vectorR[i] != 0)
		{
			NroReinas++;
		}
	}
	if ((TAM - NroReinas - 2) <= getTamValidos())
	{
		return true;
	}
	return false;
}

void Tablero::ataquesDiagonal()
{
	for (int i = 0; i < CasosDeReinas.size(); i++)
	{
		//Diagonal Superior Derecha		
		pair<int, int> CasillaDSD;
		CasillaDSD.first = CasosDeReinas[i].first;
		CasillaDSD.second = CasosDeReinas[i].second;

		//Diagonal Inferior Derecha
		pair<int, int> CasillaDID;
		CasillaDID.first = CasosDeReinas[i].first;
		CasillaDID.second = CasosDeReinas[i].second;

		//Diagonal Superior Izquierda
		pair<int, int> CasillaDSI;
		CasillaDSI.first = CasosDeReinas[i].first;
		CasillaDSI.second = CasosDeReinas[i].second;

		//Diagonal Inferior Izquierda
		pair<int, int> CasillaDII;
		CasillaDII.first = CasosDeReinas[i].first;
		CasillaDII.second = CasosDeReinas[i].second;
		for (int i = 0; i < TAM; i++)
		{
			CasillaDSD.first--;
			CasillaDSD.second++;
			if (CasillaDSD.second < TAM && CasillaDSD.first >= 1)
			{
				PosicionesNoValidas.push_back(CasillaDSD);
			}

			CasillaDID.first++;
			CasillaDID.second++;
			if (CasillaDID.first < TAM && CasillaDID.second < TAM)
			{
				PosicionesNoValidas.push_back(CasillaDID);
			}

			CasillaDSI.first--;
			CasillaDSI.second--;
			if (CasillaDSI.second >= 1 && CasillaDSI.first >= 1)
			{
				PosicionesNoValidas.push_back(CasillaDSI);
			}

			CasillaDII.first++;
			CasillaDII.second--;
			if (CasillaDII.second >= 1 && CasillaDII.first < TAM)
			{

				PosicionesNoValidas.push_back(CasillaDII);
			}
		}
	}
}

void Tablero::ataquesLineaRecta()
{
	for (int i = 1; i < TAM; i++)
	{
		if (vectorR[i] != 0)
		{
			// HORIZONTAL
			pair<int, int> horizontal;
			horizontal.first = vectorR[i];
			// VERTICAL 
			pair<int, int> vertical;
			vertical.second = i;
			for (int i = 1; i <= TAM; i++)
			{
				horizontal.second = i;
				PosicionesNoValidas.push_back(horizontal);
				vertical.first = i;
				PosicionesNoValidas.push_back(vertical);
			}
			// DATOS INICIALES
			CasillaAuxiliar.first = vectorR[i];
			CasillaAuxiliar.second = i;
			CasosDeReinas.push_back(CasillaAuxiliar);
		}
	}
}

void Tablero::depurarPosiciones()
{
	ataquesLineaRecta();
	ataquesDiagonal();
	for (int i = 0; i < PosicionesNoValidas.size(); i++)
	{
		for (int j = 0; j < Validos.size(); j++)
		{
			if ((Validos[j].first == PosicionesNoValidas[i].first && Validos[j].second == PosicionesNoValidas[i].second))
			{
				Validos.erase(Validos.begin() + j);
			}
		}
	}
}

void Tablero::ControlNoMasNodos()
{
	if (Validos.size() == 0)
	{
		Explorado = true;
	}
}

void Tablero::SacarSiguientePosibilidad()
{
	ControlNoMasNodos();
	if (Explorado != true)
	{
		srand(time(NULL));
		int num = rand() % getTamValidos();
		for (int i = 1; i < TAM; i++)
		{
			vectorRSiguientePaso[i] = vectorR[i];
		}
		vectorRSiguientePaso[Validos[num].second] = Validos[num].first;
		Validos.erase(Validos.begin() + num);
	}
}


int Tablero::devolverVectorR(int columna)
{
	return vectorRSiguientePaso[columna];
}




void Tablero::MostrarValidos()
{
	//	depurarPosiciones();
	cout << "POSICIONES DISPONIBLES" << endl;
	for (int t = 0; t < Validos.size(); t++)
	{
		cout << Validos[t].first << "," << Validos[t].second << endl;
	}
}

void Tablero::MostrarTablero()
{
	for (int i = 1; i < TAM; i++)
	{
		cout << "Reina #" << i;
		cout << " Columna : [" << i << "] Fila : [" << vectorR[i] << "]" << endl;
	}
}

