#include <iostream>
#include <ctime>
#pragma once
#include "Tablero.h"
class GrafoBFS
{
private:
	int Cantidad_Nodos;
	int Semilla[TAM];

	Tablero NodoInicial;
	vector<Tablero> Nodos;
	clock_t time_req;

	bool encontrado;
public:
	GrafoBFS(int N);
	~GrafoBFS();

	void MandarNodoPadrePersonalizado();
	void EncontrarSoluciones();
	void ComenzarBusqueda();
};

