#include <iostream>
#include <vector>
#pragma once
using namespace std;
#define TAM 6
class Tablero
{
private:
	bool Explorado;
	int vectorR[TAM];
	int vectorRSiguientePaso[TAM];
	vector<pair<int, int>> Validos;

	vector<pair<int, int>> PosicionesNoValidas;
	vector<pair<int, int>> CasosDeReinas;
	pair<int, int> CasillaAuxiliar;
public:
	Tablero();
	~Tablero();

	void mandarReinas(int InicioVector[TAM]);
	int getTamValidos();
	int getReina(int columna);
	bool EsSolucion();
	bool NoRepetido(int VectorParaVerificar[TAM]);
	bool ValidosMayoresAReinas();


	void ataquesDiagonal();
	void ataquesLineaRecta();
	void depurarPosiciones();

	void SacarSiguientePosibilidad();
	int devolverVectorR(int columna);
	void ControlNoMasNodos();
	//MOSTRAR
	void MostrarValidos();
	void MostrarTablero();
};

