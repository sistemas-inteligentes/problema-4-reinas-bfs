#include "GrafoBFS.h"

GrafoBFS::GrafoBFS(int N)
{
	Cantidad_Nodos = 0;
	encontrado = false;
	for (int i = 1; i < TAM; i++)
	{
		Semilla[i] = 0;
	}
	NodoInicial.mandarReinas(Semilla);
	Nodos.push_back(NodoInicial);
}

void GrafoBFS::MandarNodoPadrePersonalizado()
{
	for (int i = 1; i < TAM; i++)
	{
		cout << "Columna [" << i<<"] Fila : ";
		cin >> Semilla[i];
	}
	NodoInicial.mandarReinas(Semilla);
	Nodos.erase(Nodos.begin());
	Nodos.push_back(NodoInicial);
}

void GrafoBFS::ComenzarBusqueda()
{
	time_req = clock();
	while (Nodos.size() != 0 && encontrado != true)
	{
		Nodos[0].depurarPosiciones();
		if (Nodos[0].ValidosMayoresAReinas())
		{
			//CORAZON DEL PROGRAMA!!>..
			while (Nodos[0].getTamValidos() != 0 && encontrado != true)
			{
				Nodos[0].SacarSiguientePosibilidad();
				for (int j = 1; j < TAM; j++)
				{
					Semilla[j] = Nodos[0].devolverVectorR(j);
				}
				Tablero nuevoNodo;
				nuevoNodo.mandarReinas(Semilla);
				if (nuevoNodo.EsSolucion())
				{
					encontrado = true;
					cout << "Es Solucion" << endl;
					nuevoNodo.MostrarTablero();
					cout << endl;
				}
				else
				{
					int Iterador = 0;
					bool resp = true;
					while (Iterador < Nodos.size() && resp != false)
					{
						if (Nodos[Iterador].NoRepetido(Semilla))
						{
							resp = false;
						}
						Iterador++;
					}
					if (resp == true)
					{
						Nodos.push_back(nuevoNodo);
					}
				}
			}
		}
		Cantidad_Nodos++;
		//ELIMINAMOS EL QUE NO NOS IMPORTA
		Nodos.erase(Nodos.begin());

	}
	time_req = clock() - time_req;
	cout << "Without using pow function, it took " << (float)time_req / CLOCKS_PER_SEC << " seconds" << endl;

	cout << Cantidad_Nodos << endl;
}
